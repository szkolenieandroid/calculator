package com.androidbootcamp.calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class CalculatorActivity extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        initButtonClickListeners();
    }

    private void initButtonClickListeners() {
        findViewById(R.id.operation_multiply).setOnClickListener(this);
        findViewById(R.id.operation_divide).setOnClickListener(this);
        findViewById(R.id.operation_add).setOnClickListener(this);
        findViewById(R.id.operation_substract).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Integer firstOperand;
        Integer secondOperand;

        try {
            firstOperand = parseOperandValue(R.id.first_operand);
            secondOperand = parseOperandValue(R.id.second_operand);
        } catch (Exception e) {
            return;
        }


        Integer result = null;

        switch (v.getId()) {
            case R.id.operation_multiply:
                result = firstOperand * secondOperand;
                break;
            case R.id.operation_divide:
                if(secondOperand == 0) {
                    presentDivideWithZeroError();
                    return;
                }
                result = firstOperand / secondOperand;
                break;
            case R.id.operation_add:
                result = firstOperand + secondOperand;
                break;
            case R.id.operation_substract:
                result = firstOperand - secondOperand;
                break;
        }
        String resultString = result.toString();
        presentResultString(resultString);
    }

    private void presentResultString(String resultString) {
        TextView calcResultView = (TextView) findViewById(R.id.calc_result);
        calcResultView.setText(resultString);
    }

    private void presentDivideWithZeroError() {
        presentResultString("Divide with zero error!");
    }

    private Integer parseOperandValue(int operandViewId) {
        EditText operandView = (EditText) findViewById(operandViewId);
        Integer operandValue = Integer.parseInt(operandView.getText().toString());
        return operandValue;
    }

    public void goToSecond(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }
}
