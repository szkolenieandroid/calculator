package com.androidbootcamp.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        String param = getIntent().getStringExtra("param");

        TextView textView = (TextView) findViewById(R.id.text_view);
        textView.setText("Received param " + param);

    }

}
