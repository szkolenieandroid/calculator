package com.androidbootcamp.calculator;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.TextView;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class SecondActivityTest extends ActivityUnitTestCase<SecondActivity> {

    public SecondActivityTest(){
        super(SecondActivity.class);
    }

    public void testOnCreate() throws Exception {


        Intent intent = new Intent(getInstrumentation().getContext(), SecondActivity.class);
        intent.putExtra("param", "My Param");
        startActivity(intent, null, null);

        SecondActivity activity = getActivity();
        TextView textView = (TextView) activity.findViewById(R.id.text_view);
        String text = textView.getText().toString();

        assertEquals("Bundle param was not received", "Received param My Param", text);

    }
}