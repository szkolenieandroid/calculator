package com.androidbootcamp.calculator;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.widget.Button;


public class CalculatorActivityInstrumentationTest extends ActivityInstrumentationTestCase2<CalculatorActivity> {

    public CalculatorActivityInstrumentationTest() {
        super(CalculatorActivity.class);
    }

    public void testStartSecondActivity() throws Exception {

        CalculatorActivity activity = getActivity();


        Instrumentation.ActivityMonitor monitor =
                getInstrumentation().
                        addMonitor(SecondActivity.class.getName(), null, false);

        Button button = (Button) activity.findViewById(R.id.goToSecond);
        TouchUtils.clickView(this, button);

        SecondActivity secondActivity = (SecondActivity) monitor.waitForActivityWithTimeout(2000);
        assertNotNull(secondActivity);
    }
}