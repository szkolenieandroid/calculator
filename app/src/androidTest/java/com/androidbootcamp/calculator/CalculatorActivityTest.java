package com.androidbootcamp.calculator;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import junit.framework.TestCase;

public class CalculatorActivityTest extends ActivityUnitTestCase<CalculatorActivity> {

    public CalculatorActivityTest() {
        super(CalculatorActivity.class);
    }

    public void testMultiplyOperation() throws Exception {

        Intent intent = new Intent(getInstrumentation().getContext(), CalculatorActivity.class);
        startActivity(intent,null, null);
        CalculatorActivity activity = getActivity();

        ((EditText) activity.findViewById(R.id.first_operand)).setText("23");
        ((EditText) activity.findViewById(R.id.second_operand)).setText("23");
        activity.findViewById(R.id.operation_multiply).performClick();
        String calculationResult = ((TextView) activity.findViewById(R.id.calc_result)).getText().toString();
        assertEquals("Incorrect multiplying result  ", "529", calculationResult);
    }

    public void testOnClick() throws Exception {

    }
}